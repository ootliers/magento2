<?php

namespace Ootliers\Monitoring\Importer;

use Magento\Sales\Model\Order;
use Ootliers\Monitoring\Helper\Data;
use Zend\Http\Request;

class Importer
{
    const API_URL = "http://api.ootliers.com/api/v1/site/%s/orders";
    private $apiKey;
    private $siteId;

    public function __construct(Data $data)
    {
        $this->apiKey = $data->getGeneralConfig("api_key");
        $this->siteId = $data->getGeneralConfig("site_id");
    }

    public function import(Order $order)
    {
        if (empty($this->apiKey) || empty($this->siteId)) {
            return;
        }

        $items = [];

        foreach ($order->getItems() as $item) {
            $items[] = [
                'name' => $item->getName(),
                'price' => $item->getPrice(),
                'quantity' => $item->getQtyOrdered(),
            ];
        }

        $dateTime = new \DateTime($order->getCreatedAt());
        $orderInfo = [
            'order' => [
                'number' => $order->getRealOrderId(),
                'ordered_at' => $dateTime->format(\DATE_ATOM),
                'total' => $order->getGrandTotal(),
                'items' => $items
            ],
        ];
        $this->sendPayload($orderInfo);
    }

    /**
     * use Zend client as described in magento docs.
     * @param array $payload
     */
    private function sendPayload(array $payload)
    {
        $request = new Request();
        $request->getHeaders()->addHeaders([
            'Authorization' => $this->apiKey,
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ]);
        $request->setContent(json_encode($payload));
        $request->setMethod(Request::METHOD_POST);
        $request->setUri(sprintf(static::API_URL, $this->siteId));
        $client = new \Zend\Http\Client();
        $client->send($request);
  }
}
