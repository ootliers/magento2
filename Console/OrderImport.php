<?php
namespace Ootliers\Monitoring\Console;

use Magento\Reports\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Model\Order;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ootliers\Monitoring\Importer\Importer;

class OrderImport extends Command
{
   private $importer;
   private $orderCollectionFactory;

   public function __construct(Importer $importer, CollectionFactory $orderCollectionFactory)
   {
      $this->importer = $importer;
      parent::__construct(null);
       $this->orderCollectionFactory = $orderCollectionFactory;
   }

   protected function configure()
   {
       $this->setName('ootliers:order-import');
       $this->setDescription('Import orders into Ootliers');

       parent::configure();
   }
   protected function execute(InputInterface $input, OutputInterface $output)
   {
       $collection = $this->orderCollectionFactory->create()
           ->addAttributeToSelect('*');
       /** @var Order $order */
       foreach ($collection->getItems() as $order) {
           $this->importer->import($order);
       }
   }
}
