<?php

namespace Ootliers\Monitoring\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Ootliers\Monitoring\Importer\Importer;
use Psr\Log\LoggerInterface;

class OrderPlaceAfter implements ObserverInterface
{
    protected $importer;

    public function __construct(Importer $importer)
    {
        $this->importer = $importer;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            /** @var Order $order */
            $order = $observer->getEvent()->getOrder();
            $this->importer->import($order);
        } catch (\Exception $e) {
        }
    }
}
